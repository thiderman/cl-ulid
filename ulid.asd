;;;; ulid.asd

(asdf:defsystem #:ulid
  :description "Universally-Unique Lexigographically-Sortable Identifier implementation for Common Lisp"
  :author "Lowe Thiderman <lisp@lowe.thiderman.org>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "ulid")))
