;;;; ulid.lisp

;; An ULID is a Universally Unique Lexicographically Sortable Identifier

(in-package #:ulid)

;; Crockford's Base32, as by the specification.
(defconstant +encoding+ (coerce "0123456789ABCDEFGHJKMNPQRSTVWXYZ" 'list))
(defconstant +encodinglen+ (length +encoding+))
(defconstant +maxtime+ (- (expt 2 48) 1))
(defconstant +max+ (- (expt 2 80) 1))

(defvar *last-timestamp* 0)
(defvar *last-random* 0)
(defvar *lock* (bt:make-lock))

(define-condition ulid-unknown-decode-error (error)
  ((decode-character :initarg :char
                     :reader decode-character))
  (:report (lambda (condition stream)
             (format stream
                     "Cannot decode ~A (~S).~&"
                     (decode-character condition)
                     (decode-character condition))))
  (:documentation "Only characters inside of `ulid:+encoding+' can be decoded."))

(define-condition ulid-overflow-error (error)
  ((overflow :initarg :overflow :reader overflow))
  (:report (lambda (condition stream)
             (format stream "~D is larger than 2^80.~&" (overflow condition)))))

(define-condition ulid-time-overflow-error (error)
  ((overflow :initarg :overflow :reader overflow))
  (:report (lambda (condition stream)
             (format stream "~D is larger than 2^48.~&" (overflow condition)))))

(defun make-ulid (&optional (time (now)))
  "Generates a ULID.

Uses the current timestamp if none is given."
  (check-type time timestamp)
  (let ((encoded (encode-time time))
        (random (encode-randomness time)))
    (concatenate 'string encoded random)))

(defun encode-time (time)
  "Encodes the UNIX timestamp into a ULID timestamp"
  (check-type time timestamp)

  (let ((n (timestamp-as-number time)))
    (when (> n +maxtime+)
      (error 'ulid-time-overflow-error :overflow n))

    (encode n)))

(defun encode-randomness (time)
  "Generates and encodes a randomness string for use in an ULID."
  (check-type time timestamp)

  (let ((n (timestamp-as-number time)))
    (encode (randomness n) 16)))

(defun encode (n &optional (return-length 10))
  "Encode N into a Base32 string.

RETURN-LENGTH defaults to 10, which is the maximum length for the timestamp part
  of an ULID."
  (check-type n integer)
  (when (> n +max+)
    (error 'ulid-overflow-error :overflow n))
  (let ((arr (make-array (list return-length))))
    (loop for i below return-length
          collect (let* ((result (mod n +encodinglen+)))
                    (setq n (/ (- n result) +encodinglen+))
                    (setf
                     (aref arr (- return-length 1 i)) ; The -1 is
                     (nth result +encoding+))))
    (coerce arr 'string)))

(defun decode (ulid)
  "Decode a ULID string to a timestamp and the random number."
  (check-type ulid string)
  (assert (= (length ulid) 26))
  (let* ((timestamp (coerce (reverse (subseq ulid 0 10)) 'list))
         (random-data (coerce (reverse (subseq ulid 10 26)) 'list))
         ;; unix-ms since it has both the unixtime and the milliseconds attached
         (unix-ms (reduce #'+ (loop for elt in timestamp and idx from 0
                                    collect (* (decode-char elt)
                                               (expt +encodinglen+ idx)))))
         (ms (mod unix-ms 1000))
         (unix (truncate (/ unix-ms 1000)))
         (random (reduce #'+ (loop for elt in random-data and idx from 0
                                   collect (* (decode-char elt)
                                              (expt +encodinglen+ idx))))))
    (when (> unix-ms +maxtime+)
      (error 'ulid-time-overflow-error :overflow unix-ms))
    (values (unix-to-timestamp unix :nsec (* ms 1000000))
            random)))

(defun decode-char (char)
  "Decode a single character.

 If it isn't already uppercased, it will be made so. The character set for
  encoding is all uppercase."
  (let* ((upper-char (coerce (format nil "~@:(~c~)" char) 'character))
         (x (position upper-char +encoding+)))
    (unless x
      (error 'ulid-unknown-decode-error :char char))
    x))

(defun randomness (n)
  "Returns a random integer for the timestamp N."
  (check-type n integer)
  (bt:with-lock-held (*lock*)
    (if (= n *last-timestamp*)
        ;; Same timestamp as last time, increment the random by one and return it.
        (let ((new (+ 1 *last-random*)))
          (when (> new +max+)
            (error 'ulid-overflow-error :overflow *last-random*))
          (setq *last-random* new))
        ;; If it wasn't the same one, update the latest timestamp and make a new
        ;; random.
        (progn
          (setq *last-timestamp* n)
          (setq *last-random* (random +max+))))))

(defun timestamp-as-number (&optional (time (now)))
  "Returns the a time as a millisecond UNIX timestamp number.

Defaults to current time if none is provided."
  (check-type time timestamp)
  ;; First formatting directive is just the number straight up. The second
  ;; directive is to take care of 25 milliseconds becoming "025".
  (parse-integer (format nil
                         "~D~3,'0,D"
                         (timestamp-to-unix time)
                         (timestamp-millisecond time))))
