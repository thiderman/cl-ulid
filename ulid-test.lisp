(in-package #:ulid)

;; For a quick benchmark;
;; ULID> (time (loop for i from 1 to 1000000 do (make-ulid)))
;; Evaluation took:
;;   3.238 seconds of real time
;;   3.256070 seconds of total run time (3.235945 user, 0.020125 system)
;;   [ Run times consist of 0.139 seconds GC time, and 3.118 seconds non-GC time. ]
;;   100.56% CPU
;;   11,968,149,426 processor cycles
;;   3,181,859,072 bytes consed
;; ULID> (/ 1000000 3.238)
;; 308832.63

(defun ulid-benchmark ()
  "Threaded benchmark to figure out performance."
  (let* ((cpus (cpus:get-number-of-processors))
         (targets (/ 10000 cpus)))
    (loop for i from 1 to cpus do
      (bt:make-thread
       (lambda () (loop for i from 1 to targets
                   do (bt:make-thread
                       (lambda ()
                         (format t "ulid: ~S~&"  (make-ulid))))))))))

(test ulid-test
  (let ((*last-timestamp* 1568987852000)
        (*last-random* 12345678910111213)
        (ts (unix-to-timestamp 1568987852)))
    (is (string-equal (make-ulid ts) "01DN7FDB7000000AYWAHEZ5FFE"))
    (is (string-equal (make-ulid ts) "01DN7FDB7000000AYWAHEZ5FFF"))))

(test encode-time-test
  (let ((*last-timestamp* 0)
        (*last-random* 0))
    (is (string-equal
         (encode-time (unix-to-timestamp 1568987852))
         "01DN7FDB70"))))

(test encode-test
  (is (string-equal (encode 1) "0000000001"))
  (is (string-equal (encode 32) "0000000010"))
  (is (string-equal (encode +max+ 16) "ZZZZZZZZZZZZZZZZ"))
  (signals ulid-overflow-error (encode (+ 1 +max+) 16)))

(test decode-test
  ;; Without milliseconds...
  (let* ((ts (unix-to-timestamp 1569008730))
         (ulid (make-ulid ts))
         (decoded (decode ulid)))
    (is (= (timestamp-to-unix decoded)
           1569008730)))
  ;; ...and with
  (let* ((ts (timestamp+ (unix-to-timestamp 1569008730)
                         25000000 :nsec))
         (ulid (make-ulid ts))
         (decoded (decode ulid)))
    (is (= (timestamp-to-unix decoded)
           1569008730))
    (is (= (timestamp-millisecond decoded)
           25)))

  ;; Character decoding error
  (signals ulid-unknown-decode-error (decode "ZZZZZZZZZÖ0000000000000000"))
  ;; ULID length error
  (signals simple-error (decode "ZZZZ"))
  ;; Timestamp overflow error
  (signals ulid-time-overflow-error (decode "8000000000ZZZZZZZZZZZZZZZZ")))

(test random-increment-test
  (let ((*last-timestamp* 0)
        (*last-random* 0))
    ;; Test that it just increases for the first three times...
    (is (= (randomness 0) 1))
    (is (= (randomness 0) 2))
    (is (= (randomness 0) 3))
    (setq *last-random* 0)
    (is (= (randomness 0) 1))
    ;; ...and that it's something else once the timestamp changes.
    (is (not (= (randomness 4321) 1)))

    ;; Finally, test the overflow
    (setq *last-timestamp* 0)
    (setq *last-random* (- +max+ 2))
    (is (= (randomness 0) (- +max+ 1)))
    (is (= (randomness 0) +max+))
    (signals ulid-overflow-error (randomness 0))))

(test timestamp-as-number-test
  (let ((ts (unix-to-timestamp 1568987852)))
    (is (= (timestamp-as-number ts) 1568987852000))
    ;; This setup is to make sure that we have a timestamp that has milliseconds.
    ;; Weirdly, the `timestamp+' interface does not allow to set those.
    (setq ts (timestamp+ ts 25000000 :nsec))
    (is (= (timestamp-as-number ts) 1568987852025))))
